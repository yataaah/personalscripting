import gear_nodes_manager as gnm
import sys
sys.path.append('X:\\DEV\\dependencies')
from pymongo import MongoClient
import os
import shutil
import maya.cmds as cmds

def publish():
    client = MongoClient('vvc8k5', 27017)

    project = cmds.workspace(shortName = True).split("/")[-1]
    db = client.geo
    collection = db[project]

    # file path
    currentDir = os.path.dirname(cmds.file(expandName=True , q=True))
    # file name without extention
    currentFileName = os.path.basename(cmds.file(expandName=True , q=True)).split(".")[0]
    # parent dir of the file
    parentDir = os.path.abspath(os.path.join(currentDir, os.pardir))
    # # old files directory
    # oldPath = os.path.join(parentDir,"01_OLD")
    # # check if old directory exists
    # if not os.path.exists(oldPath):
    #     os.mkdir(oldPath)

    # name of the file to publish (remove build version)
    publishName = "_".join(currentFileName.split("_")[0:-1])
    
    # i = 1
    # while os.path.exists(os.path.join(oldPath,publishName) + '_v%02i' %i  + ".mb") :
    #     i += 1

    oldFileFullPath = os.path.join(parentDir,publishName) + "_old.mb"
    currentPublishPath = os.path.join(parentDir,publishName) + ".mb"

    #send to DB
    gnm.sendAllGeoToDB(collection)

    # print currentPublishPath
    # print oldFileFullPath
    # shutil.move(currentPublishPath, oldFileFullPath)
    # # # move current ref
    # # if not os.path.exists(currentPublishPath):
    # #     os.rename(currentPublishPath, oldFileFullPath)
    
    # # export new ref
    # try :
    #     cmds.file(currentPublishPath,
    #         force = True,
    #         type = "mayaBinary",
    #         options = "v=0",
    #         preserveReferences = True,
    #         exportSelected = True)
    # except Exception as e  :
    #     os.rename(oldFileFullPath, currentPublishPath)
    #     cmds.warning("couldn't reference selected, check if it already has references inside")
    #     print e



if __name__ == "__main__":
    publish()