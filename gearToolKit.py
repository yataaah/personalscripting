# GearToolKit
# Author : Anthony Tran
# Version : 0.1

import maya.cmds as cmds
import gearToolKit as gtk
import os

appdata_path = os.getenv('APPDATA')
preferences_path = appdata_path+'\\gearToolKit\\'
prefs_name = 'gtk_preferences.txt'
<<<<<<< HEAD
loadModules = {
    # Nice name for frame layout : function to build the module
}

def list_modules():
    # Will list the scripts inside the module directoy
=======

def list_modules():
>>>>>>> master
    toolkit_path = gtk.__file__
    module_path = toolkit_path.split('\\')[:-1]
    module_path = ('\\').join(module_path)
    module_list = [f[:-3] for f in os.listdir(module_path) if f[-3:] == '.py']
    module_list.remove('__init__')

    return module_list

def preferences_exists():
    if os.path.isdir(preferences_path):
        if os.path.isfile(preferences_path+prefs_name):
            return True
    return False

def get_preferences():
    if preferences_exists():
        file = open(preferences_path+prefs_name, 'r')

<<<<<<< HEAD
        # Add modules to modules

=======
>>>>>>> master
def write_preferences():
    pass

def preferences_build():
    if cmds.window('gearToolKit', exists=True):
        cmds.deleteUI('gearToolKit')
    cmds.window('gearToolKit', title='GTK Preferences')
    fl = cmds.formLayout()
    fr = cmds.frameLayout(label='Modules', bgc=[.22,.22,.22], backgroundShade=True,
        enableBackground=True)
    cmds.gridLayout(cwh=[150,40])
    
    modules = get_preferences()

    for x in modules:
        cmds.iconTextCheckBox(label=x,
            style="iconAndTextHorizontal",
            image='cube.png',
            value=True)

    cmds.setParent('..') # Close grid layout
    cmds.button(label='APPLY PREFERENCES', h=30)
    cmds.setParent('..') # Close frame layout
    cmds.setParent('..') # Close form layout

    margin = 7
    cmds.formLayout(fl, edit=True,
        attachForm=([fr, 'top', margin],
                    [fr, 'right', margin],
                    [fr, 'left', margin],
                    [fr, 'bottom', margin]))
    cmds.showWindow()

<<<<<<< HEAD
def build_gearToolKit_UI():
    cmds.columnLayout(adj=True)
    for x in loadModules:
        cmds.frameLayout(label=x, collapsable=True)
        x['command']
        cmds.setParent('..') # Close the frame layout
    cmds.setParent('..') # Close the column layout

=======
>>>>>>> master
def build_workspace_control():
    cmds.workspaceControl('gearToolKit_workspaceControl', retain=False,
        floating=True, uiScript='build_gearToolKit_UI()')
preferences_build()