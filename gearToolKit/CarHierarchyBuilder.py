# CAR HIERARCHY BUILDER by Anthony tran
# 
# 

# CAR_GROUP
# TITLES
# CONTROLLERS
# BODY
#   EXTER
#      OPTICS
#         FRONT
#         SIDES
#         REAR
#      WINDOWS
#      TECH
#      CACHES
#      EXTER_OTHERS
#   INTER
#      SEATS
#         DRIVER
#            HEAD
#            BACKREST
#            SEAT
#            BASE
#      DASHBOARD
#      PAVILLON
#         STEERING_WHEEL
#      CONSOLE_CENTRALE
#      INTER_OTHERS
#   DOORS
# WHEELS
#   WHEEL_FR
#   WHEEL_FL
#   WHEEL_BR
#   WHEEL_BL
# RECEPTION

import maya.cmds as cmds


def buildUI(mode='windowed'):
    if mode == 'windowed':
        if cmds.window('chb_ui', exists=True):
            cmds.deleteUI('chb_ui')
        cmds.window('chb_ui')
    
    cmds.   