# Compact script editor by Anthony Tran
# 
# TO DO:
# Interface
# 
# 
# 

def cse_closeTab():
    tab = cmds.tabLayout('cse_tabLayout', query=True, selectTab=True)

def cse_newScriptEditor(*args):
    index = cmds.tabLayout('cse_tabLayout', query=True, numberOfChildren=True)
    name = 'cse_fieldExe'+str(index)
    cmds.cmdScrollFieldExecuter(name,
        sourceType="python",
        showLineNumbers=True)
    cmds.tabLayout('cse_tabLayout', edit=True, selectTab=name, tabLabel=[name,'Python'])

def cse_renameTab():
    prompt = cmds.promptDialog(message='New tab name:', button=['Rename','Cancel'], defaultButton='Rename', cancelButton='Cancel')
    if prompt == 'Rename':
        name = cmds.promptDialog(query=True, text=True)
        cmds.tabLayout('cse_tabLayout', edit=True, )

def cse_newTab():
    prompt = cmds.promptDialog(message='New tab name:', button=['New Tab','Cancel'], defaultButton='New Tab', cancelButton='Cancel')
    if prompt == 'New Tab':
        name = cmds.promptDialog(query=True, text=True)
        cmds.tabLayout('cse_tabLayout', edit=True, )

def cse_buildUI(windowed=True):
    if windowed:
        if cmds.window('compactScriptEditor', exists=True):
            cmds.deleteUI('compactScriptEditor')
        cmds.window('compactScriptEditor')

    cmds.paneLayout('cse_paneLayout', configuration='horizontal2')
    # Command reporter
    cmds.cmdScrollFieldReporter('cse_feedback')
    # Command fields tabs
    cmds.tabLayout('cse_tabLayout',
        showNewTab=True,
        childResizable=True,
        tabsClosable=True,
        # Commands
        newTabCommand=cse_newScriptEditor,
        doubleClickCommand='cse_renameTab()'
        )
    cse_newScriptEditor()
    cmds.setParent('..') # Close tab layout
    cmds.setParent('..') # Close form layout
    # Scripting panel popup menu
    f=cmds.popupMenu('cse_fieldMenu', parent='cse_tabLayout')
    cmds.menuItem(dividerLabel='Tabs', divider=True, parent=f)
    cmds.menuItem(label='New tab', parent=f, command=cse_newScriptEditor)
    cmds.menuItem(label='Close tab', parent=f)
    cmds.menuItem(label='Close every other tabs', parent=f)
    cmds.menuItem(label='Clear tab', parent=f)

    cmds.menuItem(dividerLabel='File', divider=True, parent=f)
    cmds.menuItem(label='Execute', parent=f)
    cmds.menuItem(label='Open', parent=f)
    cmds.menuItem(label='Save as', parent=f)
    cmds.menuItem(label='Save selection as', parent=f)

    # Feed back panel popup menu
    p=cmds.popupMenu('cse_feedMenu', parent='cse_feedback')
    cmds.menuItem(label='Clear History', parent=p)
    cmds.menuItem(label='Echo all commands', checkBox=True, parent=p)

    if windowed:
        cmds.showWindow()
cse_buildUI()