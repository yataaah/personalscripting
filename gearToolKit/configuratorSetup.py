# Configurator Setup by Anthony Tran
# Alternate names : Configearator, Configear
# Summary : This script create a configurator setup. It allows to create,
# rename and delete configurations, branches and options for objects 
# displayability using attributes and objects groups.

# TO DO:
# - Add the version attribute to an object and connect choices to it
# - Remove branches
# - Remove options
# - Move configurations
# - Move branches
# - Move options
# - Node editor positionning

import maya.cmds as cmds
from pymel.core.windows import *

class configuratorSetup(object):
    
    def __init__(self):
        super(configuratorSetup, self)

        # Create empty variables
        self.mainCtrl = None
        self.configurations = {}
        # configurations = {'configName' : index,
        #                   'configName' : index,
        #                   'configName' : index }
        
        self.branches = {}
        # branches = { 'branchName' : ['Option0', 'Option1', 'Option2'] ,
        #              'branchName' : ['Option0', 'Option1', 'Option2'] ,
        #              'branchName' : ['Option0', 'Option1', 'Option2'] }

        # Settings
        self.autoSelectItem = True

        # Scout the scene for info at first launch
        self.gatherConfigurator()
        if __name__ == '__main__':
            self.buildUI()


    # This function will look through scene's locators for configurator attribute
    # and find config nodes. Then it will process their attributes for branches names
    # and options to add them to the class variable for later usages
    def gatherConfigurator(self):
        # Get scene's config nodes fron the config group
        if cmds.objExists('CONFIG'):
            print 'CONFIG found.'
            configs = cmds.listRelatives('CONFIG', children=True)

            if configs != []:
                print 'Config node(s) found'
                # This variable will change if at least one corresponding locator is found
                check = False
                # For every transform, look for configurator attributes
                for config in configs:
                    if cmds.attributeQuery('configurator', node=config, exists=True):
                        # If it's a config node, then get it's enum attributes and their enum list
                        if cmds.getAttr(config+'.configurator') == 'configNode':
                            self.configurations[config] = cmds.getAttr(config+'.configIndex')
                            if check is False:
                                if cmds.listAttr(config, keyable=True) is not None:
                                    for attr in cmds.listAttr(config, keyable=True):
                                        print attr
                                        # Get enum list string
                                        enumName = cmds.addAttr('{}.{}'.format(config, attr), query=True, enumName=True)
                                        # Append branch name and branch's options to branches list
                                        if enumName is not None and enumName != 'empty':
                                            enumName = enumName.split(':')
                                        else:
                                            enumName = []

                                        self.branches[attr] = enumName
                            # Change check state to not add further attributes
                            check = True

                # If no corresponding locator is found, create default config
                if check is False:
                    print 'No config node was found. Creating default.'
                    self.createConfig(name='defaultConfig')
                    cmds.confirmDialog(message='No configurator found. Default config node created.')
        else:
            # If no config node is found create default config node
            self.createConfig(name='defaultConfig')
            cmds.confirmDialog(message='No configurator found. Default config node created.')
        

    # Query string input from user to create a config node
    def nameQuery(self, nType):
        # Ask user for config name through prompt
        prompt = cmds.promptDialog(
                    message='Enter {} name:'.format(nType),
                    button=['Create','Cancel'],
                    cancelButton='Cancel',
                    defaultButton='Create')
        if prompt == 'Create':
            # Query config name
            name = cmds.promptDialog(query=True, text=True)
            if name != '':
                return name.replace(' ','_')
            else:
                cmds.error('Invalid name entered. Please try again.')
        else:
            cmds.error('Operation cancelled by user.')
            

    # This function updates the enumName of the branch attribute
    # for every config nodes
    def refreshBranchEnumName(self, branch):
        enumName = ':'.join(self.branches[branch])
        for config in self.configurations:
            cmds.addAttr('{}.{}'.format(config, branch), edit=True, enumName=enumName)
        

    # This function will look if some nodes may conflict with node creation
    # Return the errors if found. Return true if
    def performNodeExistCheck(self, nodes, nType):
        # Make a list to fill errors in
        errors = []
        # Look if node with same name exists
        for node in nodes:
            if cmds.objExists(node):
                # If so, add it to the error list
                errors.append(node)
        # If the list isn't empty, print a message for each error then end with a error message
        if errors != []:
            print '\n_____###########    ERROR REPORT  ##########_____'
            for error in errors:
                print 'A node named {} already exists. Could not perform {} creation.'.format(error, nType)
                return True
        else:
            return False        


    # ____________________________________________________________________________________________
    # EDITING ____________________________________________________________________________________

    def renameItem(self, dictionnary, nType):
        item = cmds.textScrollList(nType+'_tsl', query=True, selectItem=True)[0]
        newName = self.nameQuery(nType)
        cmds.rename(item, newName)
        # Special operations for option renaming
        if nType == 'options':
            # Get branch name
            branch = cmds.textScrollList('branches_tsl', query=True, selectItem=True)[0]
            # Rename condition node
            cmds.rename(item+'_condition', newName+'_condition')
            # Find and replace option in branch list
            for n, i in enumerate(self.branches[branch]):
                if i == item:
                    index = n
                    self.branches[branch][n] = newName
            # Rename item in configs' attributes
            for config in self.configurations:
                enumName = cmds.addAttr('{}.{}'.format(config, branch), query=True, enumName=True).split(':')
                enumName[index] = newName
                cmds.addAttr('{}.{}'.format(config, branch), edit=True, enumName=':'.join(enumName))

        # Special operations for branch renaming
        elif nType == 'branches':
            dictionnary[newName] = dictionnary.pop(item)
            cmds.rename(item+'_choice', newName+'_choice')
            for config in self.configurations:
                cmds.renameAttr('{}.{}'.format(config, item), newName)  
        # Basic operation for configuration renaming
        else:
            dictionnary[newName] = dictionnary.pop(item)
        # Rename in UI
        iIndex = cmds.textScrollList(nType+'_tsl', query=True, allItems=True ).index(item)+1
        cmds.textScrollList(nType+'_tsl', edit=True, removeIndexedItem=iIndex)
        cmds.textScrollList(nType+'_tsl', edit=True, appendPosition=[iIndex,newName])
    
    # This function changes an option's index in the internal list
    # and in every config nodes' branch attribute
    def moveOption(self, branch, oldIndex, newIndex):
        # Keep a copy of the item, delete it from the list
        # then insert it at its new place
        item = self.branches[branch][oldIndex]
        del self.branches[branch][oldIndex]
        self.branches[branch].insert(newIndex, item)
        
        # Read the list and re-set condition node term
        for index, option in enumerate(self.branches[branch]):
            # Change number of condition node
            cmds.setAttr(option+'_condition.secondTerm', index)
        # Refresh config nodes attribute
        refreshBranchEnumName(branch)


    # Edit a config index (unlock, edit and relock)
    def editConfigIndex(self, config, newIndex):
        cmds.setAttr(config+'.configIndex', lock=False)
        cmds.setAttr(config+'.configIndex', newIndex)
        cmds.setAttr(config+'.configIndex', lock=True)

    # ____________________________________________________________________________________________
    # CREATION __________________________________________________________________________________
    
    # Create a version controller attributes on an object
    def createVersionCtrl(self, node):
        
        # Add configurator attribute to remember it's the main controller
        cmds.addAttr(node, longName='configurator', dataType='string')
        cmds.setAttr(node+'.configurator', 'mainControl', type='string', lock=True)

        # Add version enum attr to hold all the configs node
        cmds.addAttr(node, longName='version', attributeType='enum', enumName=':'.join(self.configurations), keyable=True)
        self.mainCtrl = node
        # Connect version attr to all the choices attributes of the configurator
        for branch in self.branches:
            cmds.connectAttr(node+'.version', branch+'_choice.selector')
        
    # Create a new config node with branches attributes, options and connections
    def createConfig(self, name=''):
        if name == '':
            name = self.nameQuery('configuration')
        
        if self.performNodeExistCheck([name], 'configuration'):
            self.createConfig()
            cmds.error('Can not create configuration because the name is already used. Please enter another name.')

        # Look if the config group exists
        if cmds.objExists('CONFIG') is False:
            # Create if it doesn't
            print 'creating config group'
            cmds.group(empty=True, name='CONFIG')
            cmds.setAttr('CONFIG.useOutlinerColor', True)
            cmds.setAttr('CONFIG.outlinerColor', 0,.65,1)

        
        print 'creating config locator'
        # Getting their indexes
        indexes = [cmds.getAttr(config+'.configIndex') for config in self.configurations]
        # Create a new config node and add and lock an index attribute
        cmds.createNode('transform', name=name)
        for attr in ['.tx','.ty','.tz','.rx','.ry','.rz','.sx','.sy','.sz','.visibility']:
            cmds.setAttr(name+attr, lock=True, keyable=False)
        cmds.parent(name, 'CONFIG')
        # Make index
        if indexes != []:
            index = max(indexes)+1
        else:
            index = 0
            
        # Create default configurator attributes
        cmds.addAttr(name, longName='configurator', dataType='string', keyable=False)
        cmds.setAttr(name+'.configurator', 'configNode', type='string', lock=True)
        cmds.addAttr(name, longName='configIndex', attributeType='long', keyable=False, defaultValue= index)
        cmds.setAttr(name+'.configIndex', lock=True)
        cmds.setAttr(name+'.useOutlinerColor', True)
        cmds.setAttr(name+'.outlinerColor', 0,.65,1)

        # Add branches attributes and connect them to their branch's choice node
        config = cmds.listRelatives('CONFIG', children=True)
        if config is not None:
            attributes = cmds.listAttr(config[0], keyable=True)
            if attributes is not None:
                for branch in attributes:
                    options = self.branches[branch]
                    if options == []:
                        options='empty'
                    else:
                        options = ':'.join(options)
                    print options
                    cmds.addAttr(name, longName=branch, attributeType='enum', enumName=options, keyable=True)
                    cmds.connectAttr('{}.{}'.format(name, branch), '{}_choice.input[{}]'.format(branch, index), force=True)
        else:
            if self.branches != []:
                for branch in self.branches:
                    options = self.branches[branch]
                    if options == []:
                        options='empty'
                    else:
                        options = ':'.join(options)
                    print options
                    cmds.addAttr(name, longName=branch, attributeType='enum', enumName=options, keyable=True)
                    cmds.connectAttr('{}.{}'.format(name, branch), '{}_choice.input[{}]'.format(branch, index), force=True)
                    
        
        # Add the configuration to the configurations variable
        self.configurations[name] = index
        # Add the configuration to the UI
        if cmds.textScrollList('configurations_tsl', exists=True):
            cmds.textScrollList('configurations_tsl', edit=True, append=name)


    # Create a new branch's nodes and connections
    def createBranch(self, name=''):
        # Query name from UI
        if name == '':
            name = self.nameQuery('branch')
        # Look for clashing nodes
        if self.performNodeExistCheck([name, name+'_choice'], 'branch'):
            self.createBranch()
            cmds.error('Can not create branch because the name is already in use. Please enter another name.')

        # Create group and choice nodes
        cmds.group(empty=True, name=name)
        cmds.createNode('choice', name=name+'_choice')
        cmds.addAttr(name, longName='configurator', dataType='string')
        cmds.setAttr(name+'.configurator', 'condition', type='string')
        cmds.setAttr(name+'.useOutlinerColor', True)
        cmds.setAttr(name+'.outlinerColor', 1,.65,0)

        # Add branch's attribute to config nodes and connect attribute to choice
        for config in self.configurations:
            print config, name
            cmds.addAttr(config, longName=name, attributeType='enum', enumName='empty', keyable=True)
            cmds.connectAttr('{}.{}'.format(config, name), '{}_choice.input[{}]'.format(name, self.configurations[config]), force=True)

        # Connect to main control if it exists
        if self.mainCtrl is not None:
            if cmds.objExists(self.mainCtrl):
                cmds.connectAttr(self.mainCtrl+'.version', name+'_choice.selector')

        self.branches[name] = []
        # Add the configuration to the UI
        if cmds.textScrollList('branches_tsl', exists=True):
            cmds.textScrollList('branches_tsl', edit=True, append=name, selectItem=name)
            self.changeBranch()


    # Create a new option group and edit branch's attributes
    def createOption(self, name=''):
        if cmds.textScrollList('branches_tsl', query=True, allItems=True) is None:
            cmds.confirmDialog(message='You must create a branch first.')
            return None
        branchSelection= cmds.textScrollList('branches_tsl', query=True, selectItem=True)
        if branchSelection is None:
            cmds.confirmDialog(message='You must select a branch first.')
            return None
        if name == '':
            name = self.nameQuery('option')
        if self.performNodeExistCheck([name, name+'_condition'],'option'):
            self.createOption()
            cmds.error('Can not create option because the name is already in use. Please enter another name.')
        # Query option's branch
        branch = cmds.textScrollList('branches_tsl', query=True, selectItem=True)[0]

        # Create the option group with its attributes   
        index = len(self.branches[branch])
        cmds.group(empty=True, name=name, parent=branch)
        cmds.addAttr(name, longName='configurator', dataType='string', keyable=False)
        cmds.setAttr(name+'.configurator', 'option', type='string', lock=True)
        cmds.addAttr(name, longName='optionIndex', attributeType='long', keyable=False)
        cmds.setAttr(name+'.optionIndex', index, lock=True)
        cmds.setAttr(name+'.useOutlinerColor', True)
        cmds.setAttr(name+'.outlinerColor', 1,.65,0)

        # Create condition node
        condition = cmds.createNode('condition', name=name+'_condition')
        cmds.addAttr(condition, longName='configurator', dataType='string')
        cmds.setAttr(condition+'.configurator', 'condition', type='string')
        cmds.setAttr(condition+'.secondTerm', index)
        cmds.setAttr(condition+'.colorIfTrueR' ,1)
        cmds.setAttr(condition+'.colorIfFalseR',0)

        # Connect nodes
        cmds.connectAttr(branch+'_choice.output', condition+'.firstTerm')
        cmds.connectAttr(condition+'.outColorR', name+'.visibility')

        # Add option to the branch variable and to UI
        self.branches[branch].append(name)
        if cmds.textScrollList('options_tsl', exists=True):
            cmds.textScrollList('options_tsl', edit=True, append=name)

        # Update config attribute
        for config in self.configurations:
            cmds.addAttr('{}.{}'.format(config, branch), edit=True, enumName=':'.join(self.branches[branch]))


    # ____________________________________________________________________________
    # DELETING ___________________________________________________________________

    def deleteConfig(self):
        # Disconnect config
        config = cmds.textScrollList('configurations_tsl', query=True, selectItem=True)[0]
        cIndex = self.configurations[config]
        for branch in self.branches:
            cmds.disconnectAttr('{}.{}'.format(config, branch), '{}_choice.input[{}]'.format(branch,+self.configurations[config]))
        cmds.delete(config)

        # Delete config node and delete it from dictionnary
        cmds.textScrollList('configurations_tsl', edit=True, removeItem=config)
        self.configurations.pop(config, None)

        # Move next config up in the choices input
        for c in self.configurations:
            if self.configurations[c] > cIndex:
                oldIndex = self.configurations[c]
                self.configurations[c] -= 1
                self.editConfigIndex(c, self.configurations[c])
                for branch in self.branches:
                    try:
                        cmds.disconnectAttr('{}.{}'.format(c, branch), '{}_choice.input[{}]'.format(branch,+oldIndex))
                    except:
                        a=0
                    cmds.connectAttr('{}.{}'.format(c, branch), '{}_choice.input[{}]'.format(branch,+self.configurations[c]), force=True)
        
        
    def deleteBranch(self):
        # Warning with confirm dialog before deleteing
        branch = cmds.textScrollList('branches_tsl', query=True, selectItem=True)[0]
        confirm = cmds.confirmDialog(message='Are you sure you want to delete the branch "{}".'.format(branch), button=['Confirm','Cancel'],
                    defaultButton='Confirm', cancelButton='Cancel')
        if confirm == 'Confirm':            
            # Delete attribute from configs
            for config in self.configurations:
                cmds.deleteAttr(config, attribute=branch)

            # Reparent options geo if asked by user
                # Get geometry from options
            geometry = []
            for option in self.branches[branch]:
                if cmds.listRelatives(option, children=True):
                    for obj in cmds.listRelatives(option, children=True):
                        geometry.append(obj)
            print geometry
            if geometry != []:
                    # Get branch parent
                parent = cmds.listRelatives(branch, parent=True)
                print parent
                    # Parent under branch parent or under world
                if parent is not None:                    
                    cmds.group(geometry, name=branch+'_geometry', parent = cmds.listRelatives(branch, parent=parent[0]))
                else:
                    cmds.group(geometry, name=branch+'_geometry', world=True)
            
            # Delete choice node and options
            cmds.delete(branch)
            # Remove from interface
            cmds.textScrollList('branches_tsl', edit=True, removeItem=branch)
            cmds.textScrollList('options_tsl', edit=True, removeAll=True)

        
    def deleteOption(self):
        pass
        # Warning with confirm dialog before deleting


    # ____________________________________________________________________________
    # INTERFACE RELATED __________________________________________________________

    def changeBranch(self):
        branch = cmds.textScrollList('branches_tsl', query=True, selectItem=True)
        cmds.textScrollList('options_tsl', edit=True, removeAll=True)
        if branch is None:
            return None
        else:
            branch = branch[0]
        if self.branches[branch] != ['empty']:
            cmds.textScrollList('options_tsl', edit=True, append=self.branches[branch])


    def selectItem(self, nType):
        if self.autoSelectItem:
            if nType == 'branches':
                branch = cmds.textScrollList('branches_tsl', query=True, selectItem=True)
                cmds.textScrollList('options_tsl', edit=True, removeAll=True)
                if branch is None:
                    return None
                else:
                    branch = branch[0]
                if self.branches[branch] != ['empty']:
                    cmds.textScrollList('options_tsl', edit=True, append=self.branches[branch])

            item = cmds.textScrollList(nType+'_tsl', query=True, selectItem=True)
            if item is None:
                return None
            else:
                item = item[0]
            cmds.select(item)
        else:
            return None

    # ____________________________________________________________________________
    # BUILD INTERFACES ___________________________________________________________

    # This function will build a window
    def buildUI(self, mode='windowed'):
        if mode == 'windowed' :
            if cmds.window('configuratorSetup_window', exists=True):
                cmds.deleteUI('configuratorSetup_window')

            cmds.window('configuratorSetup_window', toolbox=True, title='Configurator')

        m=cmds.formLayout(w=200,h=75)
        p=cmds.paneLayout(configuration='vertical3',w=150,h=75)

        for element in ['configurations', 'branches', 'options']:
            f=cmds.formLayout(element+'_formLayout',w=50)
            t = cmds.text(label=element.capitalize())
            s = cmds.textScrollList(element+'_tsl')
            g = cmds.rowLayout(nc=4, cw4=(15,15,15,15))
            cmds.button(element+'Add',h=15,w=15, label='+')
            cmds.button(element+'Del',h=15,w=15, label='-')
            # cmds.button(element+'MoveUp',h=15,w=15, label='^')
            # cmds.button(element+'MoveDown',h=15,w=15, label='v')
            cmds.setParent('..') # close grid layout
            cmds.setParent('..') # close form layout

            cmds.formLayout(f, edit=True,
                        attachForm=[(t,'top',3),(t,'left',3),
                                    (s,'top',23),(s,'bottom',3),(s,'left',3),(s,'right',3),
                                    (g,'top',3),(g,'right',3)]
            )

        # Add configurations to config text scroll list
        for config in self.configurations:
            cmds.textScrollList('configurations_tsl', edit=True, appendPosition=[self.configurations[config]+1,config])
        # Add branches to branches text scroll list
        for branch in self.branches:
            cmds.textScrollList('branches_tsl', edit=True, append=branch)

        # Add commands
            # Configuration
        cmds.textScrollList('configurations_tsl', edit=True, selectCommand=Callback(self.selectItem, 'configurations'))
        cmds.textScrollList('configurations_tsl', edit=True, doubleClickCommand=Callback(self.renameItem, self.configurations, 'configurations'))
        cmds.button('configurationsAdd', edit=True, command=Callback(self.createConfig ))
        cmds.button('configurationsDel', edit=True, command=Callback(self.deleteConfig ))

            # Branches
        cmds.textScrollList('branches_tsl', edit=True, selectCommand=Callback(self.selectItem, 'branches'))
        cmds.textScrollList('branches_tsl', edit=True, doubleClickCommand=Callback(self.renameItem, self.branches, 'branches'))
        cmds.button('branchesAdd', edit=True, command=Callback(self.createBranch))
        cmds.button('branchesDel', edit=True, command=Callback(self.deleteBranch), enable=False)

            # Options
        cmds.textScrollList('options_tsl', edit=True, selectCommand=Callback(self.selectItem, 'options'))
        cmds.textScrollList('options_tsl', edit=True, doubleClickCommand=Callback(self.renameItem, '', 'options'))
        cmds.button('optionsAdd', edit=True, command=Callback(self.createOption), enable=True)
        cmds.button('optionsDel', edit=True, command=Callback(self.createOption), enable=False)

        cmds.setParent('..') # close pane layout
        cmds.setParent('..') # close form layout

        cmds.formLayout(m, edit=True, attachForm=[(p,'top', 0),(p,'left', 0),(p,'right', 0),(p,'bottom', 0)])

        if mode == 'windowed' :
            cmds.showWindow('configuratorSetup_window')


    # This function will build the ui in a form layout
    def buildWorkspaceContol(self):
        cmds.workspaceControl('configuratorSetup', retain=False, floating=True, uiScript=self.buildUI)
configurator = configuratorSetup()
