'''
Changelog:
v1: First iteration, connect 1 attribute to an other one based on selection
v2: Connect multiple attributes to others separated with commas, as arrays
V3: Right click popup menu on fields
	Auto fill attributes in the popup menu
	Press enter to execute commands
<<<<<<< HEAD
V4: Right click popup menu attributes classed into submenus
	Reorganized interface
	Importable UI
=======
>>>>>>> master
'''


import maya.cmds as cmds
import functools

<<<<<<< HEAD
connect_attributes_recent = []
recent_use_length = 7

def check_selection():
	selection = cmds.ls(selection=True, long=True)
=======
def checkSelection():
	selection = cmds.ls(selection=True)
>>>>>>> master
	if selection:
		if len(selection) == 1:
			cmds.error('Select at least 2 nodes.')
		else:
			return selection
	else:
		cmds.error('Selection is empty.')

def connectAttributes_AT(*args):
<<<<<<< HEAD
	sel = check_selection()
=======
	sel = checkSelection()
>>>>>>> master
	src = cmds.textField('attributeFrom', query=True, text=True).replace(' ','').split(',')
	tgt = cmds.textField('attributeTo', query=True, text=True).replace(' ','').split(',')

	for s in sel:
		if s != sel[0]:
			if len(src) == len(tgt):
				for a in range(len(src)):
<<<<<<< HEAD
					cmds.connectAttr('{}.{}'.format(sel[0],src[a]),
						'{}.{}'.format(s,tgt[a]), force=True)
			else:
				cmds.error('The source and destination attributes must be of same length. Eg: from "translateX,translateY" to "rotateX,rotateY".')

def update_recent_uses(strings, *args):
	if isinstance(strings, basestring):
		strings = [strings]
	for string in strings:
		if len(connect_attributes_recent) > recent_use_length:
			del(connect_attributes_recent[6])
		connect_attributes_recent.insert(0, string)
=======
					cmds.connectAttr('{}.{}'.format(sel[0],src[a]), '{}.{}'.format(s,tgt[a]), force=True)
			else:
				cmds.error('The source and destination attributes must be of same length. Eg: from "translateX,translateY" to "rotateX,rotateY".')

>>>>>>> master

def fill_field(string, field, *args):
	actualText = cmds.textField(field, query=True, text=True)
	if actualText != '':
    		actualText += ','
<<<<<<< HEAD
	update_recent_uses(string)
=======
>>>>>>> master
	cmds.textField(field, edit=True, text=actualText+string)

def popup_menu_fillup(popupMenu, selIndex, field, *args):
	# Gets attributes from the first selected object
	cmds.popupMenu(popupMenu, edit=True, deleteAllItems=True)
<<<<<<< HEAD
	selection = check_selection()[selIndex]
	attributes = cmds.listAttr(selection, keyable=True, connectable=True)

	defaults = cmds.menuItem(label='Defaults', subMenu=True, parent=popupMenu)
	custom = cmds.menuItem(label='User Attributes', subMenu=True, parent=popupMenu)
	cmds.menuItem(divider=True, dividerLabel='Recently used', parent=popupMenu)

	for attribute in attributes:
		cmds.menuItem(label=attribute, parent=defaults, 
			command=functools.partial(fill_field, attribute, field))

def build_UI(windowed=True, margin={'top':5, 'left':5, 'right':5, 'bottom':5},
		*args):
	ui = 'connectAttribute'

	if windowed:
		if cmds.window(ui, exists=True):
			cmds.deleteUI(ui)

		cmds.window(ui, title='Connect Attributes', sizeable=True)
	f=cmds.formLayout()

	fr = cmds.textField('attributeFrom', placeholderText='From',
			alwaysInvokeEnterCommandOnReturn=True,
			enterCommand=connectAttributes_AT, h=25)
	to = cmds.textField('attributeTo', placeholderText='To',
			alwaysInvokeEnterCommandOnReturn=True,
			enterCommand=connectAttributes_AT, h=25)
	bt = cmds.button(w=40, h=20, label='APPLY', command=connectAttributes_AT)

	cmds.popupMenu('from_popupMenu',
		postMenuCommand=functools.partial(popup_menu_fillup,
				'from_popupMenu', 0, 'attributeFrom'),
		parent='attributeFrom')

	cmds.popupMenu('to_popupMenu',
		postMenuCommand=functools.partial(popup_menu_fillup,
				'to_popupMenu', 1, 'attributeTo'),
		parent='attributeTo')

	cmds.formLayout(f, edit=True,
		attachForm=[(fr, 'top', margin['top']), 
					(fr, 'left', margin['left']), 
					(to, 'left', margin['left']),
					(to, 'bottom', margin['bottom']),
					(bt, 'top', margin['top']),
					(bt, 'right', margin['right']), 
					(bt, 'bottom', margin['bottom'])],
		attachControl=[(fr, 'right', 3, bt), 
					   (to, 'right', 3, bt)])

	cmds.setParent('..')
	if windowed:
		cmds.showWindow()

build_UI()
=======
	selection = checkSelection()[selIndex]
	attributes = cmds.listAttr(selection, keyable=True, connectable=True)

	for attribute in attributes:
		cmds.menuItem(label=attribute, command=functools.partial(fill_field, attribute, field), parent=popupMenu)

ui = 'connectAttribute'

if cmds.window(ui, exists=True):
	cmds.deleteUI(ui)

win = cmds.window(ui, title='Connect Attributes', sizeable=True)
cmds.columnLayout(adj=True)

cmds.text(label='Select the driver object then\nall the driven objects to connect.', h=25)
cmds.textField('attributeFrom',placeholderText='From', alwaysInvokeEnterCommandOnReturn=True, enterCommand=connectAttributes_AT)
cmds.textField('attributeTo', placeholderText='To', alwaysInvokeEnterCommandOnReturn=True, enterCommand=connectAttributes_AT)
cmds.button(w=75, label='APPLY', command=connectAttributes_AT)

cmds.popupMenu('from_popupMenu', postMenuCommand=functools.partial(popup_menu_fillup, 'from_popupMenu', 0, 'attributeFrom'), parent='attributeFrom')
cmds.popupMenu('to_popupMenu', postMenuCommand=functools.partial(popup_menu_fillup, 'to_popupMenu', 1, 'attributeTo'), parent='attributeTo')

cmds.setParent('..')
cmds.showWindow(win)

>>>>>>> master
