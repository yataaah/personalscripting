<<<<<<< HEAD
import maya.cmds as cmds
import functools

def clear_list(*args):
	cmds.textScrollList('listToExport', edit=True, removeAll=True)

def exportList(*args):
	cmds.select(clear=True)
	objList = cmds.textScrollList('listToExport', query=True, allItems=True)
	if objList is None:
		cmds.error('Selection is empty.')
	for obj in objList:
		cmds.select(obj, add=True)
	cmds.ExportSelection()

def selectList(*args):
	objList = cmds.textScrollList('listToExport', query=True, allItems=True)
	for obj in objList:
		cmds.select(obj, add=True)

def add_remove_obj(action, *args):
	selection = cmds.ls(selection=True, long=True)
	actualList = cmds.textScrollList('listToExport', query=True, allItems=True)

	if action=='add':
		for item in selection:
			if actualList is None or item not in actualList:
				cmds.textScrollList('listToExport', edit=True, append=item)
	elif action=='remove':
		for item in selection:
			try:
				if item in actualList:
					cmds.textScrollList('listToExport', edit=True, removeItem=item)
			except:
				continue

def build_ui(windowed=True, margin={'top':7, 'left':7, 'right':7, 'bottom':7}):

	es_name = 'exportList'
	if windowed:
		if cmds.window(es_name, exists=True):
			cmds.deleteUI(es_name)
		cmds.window(es_name, title='Export List')

	cmds.formLayout('mainForm_LT', numberOfDivisions=100)
	cmds.textScrollList('listToExport',w=200,h=100)
	cmds.button('exportBT', label='EXPORT', command=exportList)
	cmds.button('addBT', label='ADD', command=functools.partial(add_remove_obj, 'add'))
	cmds.button('remBT', label='REMOVE', command=functools.partial(add_remove_obj, 'remove'))
	cmds.setParent('..')

	cmds.popupMenu('exportMenu', parent='exportBT')
	cmds.menuItem(label='Select list', command=selectList, parent='exportMenu')
	cmds.menuItem(label='Clear list', command=clear_list, parent='exportMenu')

	cmds.formLayout('mainForm_LT', edit=True,
		attachForm=[('listToExport','top',margin['top']),
					('listToExport','right',margin['right']),
					('listToExport','left',margin['left']),
					('listToExport','bottom',27+margin['bottom']),
					('exportBT','bottom',margin['bottom']),
					('exportBT','right',margin['right']),
					('addBT','bottom',margin['bottom']),
					('addBT','left',margin['left']),
					('remBT','bottom',margin['bottom'])],
		attachPosition=[('addBT','right',2,33),
						('remBT','left',2,33),
						('remBT','right',2,66),
						('exportBT','left',2,66)])

	if windowed:
		cmds.showWindow()
build_ui()
=======
from pymel.all import *
import maya.cmds as cmds

class exportList(object):

	"""docstring for exportList"""
	def __init__(self):
		super(exportList, self).__init__()
		self.buildExportList()

	def clearList(self,args):
		cmds.textScrollList('listToExport', edit=True, removeAll=True)

	def exportList(self,args):
		cmds.select(clear=True)
		objList = cmds.textScrollList('listToExport', query=True, allItems=True)
		if objList is None:
		    cmds.error('Selection is empty.')
		for obj in objList:
			cmds.select(obj, add=True)
		cmds.ExportSelection()

	def selectList(self,args):
		objList = cmds.textScrollList('listToExport', query=True, allItems=True)
		for obj in objList:
			cmds.select(obj, add=True)

	def addRemoveObj(self,action):
		selection = cmds.ls(selection=True)
		actualList = cmds.textScrollList('listToExport', query=True, allItems=True)

		if action=='add':
			for item in selection:
				if actualList is None or item not in actualList:
					cmds.textScrollList('listToExport', edit=True, append=item)
		elif action=='remove':
			for item in selection:
				try:
					if item in actualList:
						cmds.textScrollList('listToExport', edit=True, removeItem=item)
				except:
					continue

	def buildExportList(self):

		es_name = 'exportList'

		if cmds.window(es_name, exists=True):
			cmds.deleteUI(es_name)

		win=cmds.window(es_name, title='Export List')
		cmds.formLayout('mainForm_LT', numberOfDivisions=100)
		cmds.textScrollList('listToExport',w=200,h=100)
		cmds.button('exportBT', label='EXPORT', command=self.exportList)
		cmds.button('addBT', label='ADD', command=Callback(self.addRemoveObj, 'add'))
		cmds.button('remBT', label='REMOVE', command=Callback(self.addRemoveObj, 'remove'))
		cmds.popupMenu('exportMenu', parent='exportBT')
		cmds.menuItem(label='Select list', command=self.selectList, parent='exportMenu')
		cmds.menuItem(label='Clear list', command=self.clearList, parent='exportMenu')

		cmds.formLayout('mainForm_LT', edit=True,
			attachForm=[('listToExport','top',33),('listToExport','right',5),('listToExport','left',5),('listToExport','bottom',33),('exportBT','right',5),('exportBT','left',5),('exportBT','bottom',5),('addBT','left',5),('addBT','top',5),('remBT','right',5),('remBT','top',5)],
			attachPosition=[('addBT','right',2,50),('remBT','left',2,50)])

		cmds.showWindow()
exportList = exportList()
>>>>>>> master
