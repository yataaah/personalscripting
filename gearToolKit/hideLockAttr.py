# Hide and/or lock attribues of objects
import maya.cmds as cmds
<<<<<<< HEAD
import functools
=======
from pymel.core import *

>>>>>>> master


def hideLockAttr(attr):
	sel  = cmds.ls(selection=True)
	hide = not cmds.radioButtonGrp('hideRBG', query=True, select=True)-1
	lock = cmds.radioButtonGrp('lockRBG', query=True, select=True)-1
	error= []

	print hide, lock

	if 'all' in attr:
		if attr == 'all':
			for s in sel:
				for a in ['translate','rotate','scale']:
					for b in ['X','Y','Z']:
						try:
							cmds.setAttr('{}.{}{}'.format(s,a,b), lock=lock, keyable=hide)
						except:
							error.append('{}.{}{}'.format(s,a,b))
		else:
			attr = attr.replace('all_','')
			for s in sel:
				for b in ['X','Y','Z']:
					try:
						cmds.setAttr('{}.{}{}'.format(s,attr,b), lock=lock, keyable=hide)
					except:
						error.append('{}.{}{}'.format(s,attr,b))

	else:
		for s in sel:
			try:
				cmds.setAttr('{}.{}'.format(s,attr), lock=lock, keyable=hide)
			except:
				error.append('{}.{}'.format(s,attr))

	# ERROR REPORT
	if error:
		print '\n/// Could not modify those attributes:'
		for e in error:
			print e
		print '/// End of errors report'
		cmds.warning('Some attributes could not be modified, check script editor for more information.')



# UI
<<<<<<< HEAD
def hideLock_UI(windowed=True, margin={'top':5, 'left':5, 'right':5, 'bottom':5}):
	nm = 'hideLock_UI'

	if windowed:
		if cmds.window(nm, exists=True):
			cmds.deleteUI(nm)
		win = cmds.window(nm, title='Hide/lock attributes', toolbox=True, sizeable=False)
=======
def hideLock_UI():
	nm = 'hideLock_UI'

	if cmds.window(nm, exists=True):
		cmds.deleteUI(nm)

	win = cmds.window(nm, title='Hide/lock attributes', toolbox=True, sizeable=False)
>>>>>>> master
	cmds.formLayout('mainForm')

	r1= cmds.radioButtonGrp('hideRBG', numberOfRadioButtons=2, label1='Unhide', label2='Hide', cw=[(1,60),(2,60)], select=2) 
	r2= cmds.radioButtonGrp('lockRBG', numberOfRadioButtons=2, label1='Unlock', label2='Lock', cw=[(1,60),(2,60)], select=2) 

	# Translate
	t =cmds.rowLayout(nc=5, columnAlign=[(1,'left')])
	cmds.text(label='Translate', w=50)
<<<<<<< HEAD
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=functools.partial(hideLockAttr,'translateX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=functools.partial(hideLockAttr,'translateY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=functools.partial(hideLockAttr,'translateZ'))
	cmds.button(w=50,label = 'All', command=functools.partial(hideLockAttr,'all_translate'))
=======
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=Callback(hideLockAttr,'translateX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=Callback(hideLockAttr,'translateY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=Callback(hideLockAttr,'translateZ'))
	cmds.button(w=50,label = 'All', command=Callback(hideLockAttr,'all_translate'))
>>>>>>> master
	cmds.setParent('..')

	# Rotate
	r =cmds.rowLayout(nc=5, columnAlign=[(1,'left')])
	cmds.text(label='Rotate', w=50)
<<<<<<< HEAD
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=functools.partial(hideLockAttr,'rotateX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=functools.partial(hideLockAttr,'rotateY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=functools.partial(hideLockAttr,'rotateZ'))
	cmds.button(w=50,label = 'All', command=functools.partial(hideLockAttr,'all_rotate'))
=======
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=Callback(hideLockAttr,'rotateX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=Callback(hideLockAttr,'rotateY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=Callback(hideLockAttr,'rotateZ'))
	cmds.button(w=50,label = 'All', command=Callback(hideLockAttr,'all_rotate'))
>>>>>>> master
	cmds.setParent('..')

	# Scale
	s =cmds.rowLayout(nc=5, columnAlign=[(1,'left')])
	cmds.text(label='Scale', w=50)
<<<<<<< HEAD
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=functools.partial(hideLockAttr,'scaleX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=functools.partial(hideLockAttr,'scaleY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=functools.partial(hideLockAttr,'scaleZ'))
	cmds.button(w=50,label = 'All', command=functools.partial(hideLockAttr,'all_scale'))
	cmds.setParent('..')

	a = cmds.button(w=50,label='All', command=functools.partial(hideLockAttr,'all'))
=======
	cmds.button(w=25,label = 'X', bgc=[.8 ,.3 ,.3], command=Callback(hideLockAttr,'scaleX'))
	cmds.button(w=25,label = 'Y', bgc=[.3 ,.8 ,.3], command=Callback(hideLockAttr,'scaleY'))
	cmds.button(w=25,label = 'Z', bgc=[.2 ,.4 , 1], command=Callback(hideLockAttr,'scaleZ'))
	cmds.button(w=50,label = 'All', command=Callback(hideLockAttr,'all_scale'))
	cmds.setParent('..')

	a = cmds.button(w=50,label='All', command=Callback(hideLockAttr,'all'))
>>>>>>> master

	cmds.setParent('..')

	cmds.formLayout('mainForm', edit=True,
		attachForm=[(r1,'top' ,5),(r1,'left' ,5),
					(r2,'top',5),
					(a,'right',5),(a,'bottom',5),
					(t,'left',5),
					(r,'left',5),
					(s,'left',5),(s,'bottom',5)],
		attachControl=[(t,'top',5,r1),(a,'top',5,r1),(r2,'left',5,r1),(r,'top',5,t),(s,'top',5,r),(a,'left',5,t)])
	cmds.window(win, edit=True, w=10,h=10)
<<<<<<< HEAD
	
	if windowed:
		cmds.showWindow()
=======
	cmds.showWindow(win)
>>>>>>> master

hideLock_UI()





