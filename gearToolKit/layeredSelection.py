# Layered selection by Anthony Tran
# 
# TO DO:
# DONE Select shader related
# DONE Select group related
# DONE Select non shaded
# DONE Select with a volume (onlyIfCenterInside TO DO)
# Select by bounding box
# Select by number of faces
# Select by bounding box size
# Select by position (absolute/relative)
# Select mirror objects

# DONE Select faces by normal angle
# Select faces by area
# Select faces by number of faces
# Select edges by length

def edgesByLength(objects=None, length=0.1):
    if objects is None:
        selection = cmds.ls(selection=True)
    if length == 0.1:
        # Get length from UI
        cmds.floatField('edgeLength_ffld', query=True, value=True)
    pass
    
def buildUI(mode='windowed'):
    # Create interface in window if windowed mode is specified
    if mode == 'windowed':
        if cmds.window('layeredSelectionUI', exists=True):
            cmds.deleteUI('layeredSelectionUI')
    
    f=cmds.formLayout()

# SELECT WITH VOLUME ___________________________________________________________
def selectWithVolume(volume, onlyIfCenterInside=True):
    # This function will select all objects that are inside
    # a given volume.
    # WARNING : The onlyIfCenterInside does not work yet
    bbox = cmds.exactWorldBoundingBox(volume)

    shapes = cmds.ls(type='mesh', long=True)
    inVolume = []
    for shape in shapes:
        center = cmds.objectCenter(shape)
        if isPointInsideVolume(center, bbox):
            transform = cmds.listRelatives(shape, parent=True, fullPath=True)[0]
            inVolume.append(transform)

    cmds.select(inVolume)
        
def isPointInsideVolume(point, volume_boundingBox):
    if isNumberBetween(point[0], volume_boundingBox[0], volume_boundingBox[3]): # Compair X
        if isNumberBetween(point[1], volume_boundingBox[1], volume_boundingBox[4]): # Compair Y
            if isNumberBetween(point[2], volume_boundingBox[2], volume_boundingBox[5]): # Compair Z
                return True
    return False

def isNumberBetween(number, vMin, vMax):
    return True if vMin < number < vMax else False
# SELECT WITH VOLUME ___________________________________________________________ END


def selectByBoundingBoxSize(size):
    volume=1
    sizeX=2
    sizeY=3
    sizeZ=4

    boundingBoxes = []
    for shape in cmds.ls(type='mesh'):
        transform = cmds.listRelatives(shape, parent=True)
        bbox = cmds.exactWorldBoundingBox(shape)
        x = bbox[3]-bbox[0]
        y = bbox[4]-bbox[1]
        z = bbox[5]-bbox[2]
        vol = x*y*z
        size = [transform, vol, x, y, z]

        

def selectGroupRelated(allChildren=False):
    # This function will select all the objects at the same
    # hierarchy level as the selected objects. The allChildren flag
    # will select every object on the levels under the current one
    selection = cmds.ls(selection=True)
    parents = []
    for obj in selection:
        parent = cmds.listRelatives(obj, parent=True)
        if parent:
            parents.append(parent[0])

    for parent in parents:
        children = cmds.listRelatives(parent, children=True, allDescendents=allChildren)
        cmds.select(children, add=True)

def selectByShader(selectMode=None):
    # This function will select objects based on the shaders. It has
    # 3 modes of selection: "shaded" will select all the shaded objects
    # "nonShaded" will do the opposite and "related" will select objects
    # with the same shaders as the selected objects

    if selectMode is None:
        cmds.error('The "selectMode" flag is required. Its argument can be "shaded", "nonShaded" or "related".')
    
    # Select shader related objects or the selected objects
    elif selectMode == 'related':
        selection = cmds.ls(selection=True)
        shaders = []
        # Get shaders from selection
        for obj in selection:
            shape = cmds.listRelatives(obj, type='shape')[0]
            shader = cmds.listConnections(shape, type='shadingEngine')
            if shader:
                shaders.append(shader[0])
        
        # Select objects from shaders
        for shader in shaders:
            objs = cmds.listConnections(shader, type='mesh')
            if objs:
                cmds.select( objs, add=True )

    # Select all scene objects if shaded or non shaded
    elif selectMode == 'shaded' or selectMode == 'nonShaded':
        cmds.select(clear=True)
        shapes = cmds.ls(type='mesh')
        for shape in shapes:
            shader = cmds.listConnections(shape, type='shadingEngine')
            
            if selectMode == 'shaded':
                if shader:
                    transform = cmds.listRelatives(shape, parent=True)[0]
                    cmds.select(transform, add=True)
            elif selectMode == 'nonShaded':
                if shader is None:
                    transform = cmds.listRelatives(shape, parent=True)[0]
                    cmds.select(transform, add=True)
    else:
        cmds.error(str(selectMode) + ' is not a valid argument for the "selectMode" flag.  Its argument can be "shaded", "nonShaded" or "related".')


def floatDifference(a,b,interval):
    if -interval< a-b < interval:
        return True
    else:
        return False

def convertStringToFace(index, face):
    v = face.split(' ')[-3:]
    f = [index, float(v[0]), float(v[1]), float(v[2])]
    return f

# This function will select faces based on the first selected face's normal
# of the selection
def selectByNormalAngle():
    sel = cmds.ls(selection=True)[0]
    selectedFace = convertStringToFace('x',cmds.polyInfo(sel, faceNormals=True)[0])

    objFaces=[]
    for num, face in enumerate(cmds.polyInfo(sel.split('.')[0], faceNormals=True)):
        objFaces.append( convertStringToFace(num, face) )

    # Comparison
    matches=[]
    for face in objFaces:
        x = floatDifference(face[1], selectedFace[1], .5)
        y = floatDifference(face[2], selectedFace[2], .5)
        z = floatDifference(face[3], selectedFace[3], .5)
        if x and y and z:
            matches.append(face[0])
    for match in matches:
        cmds.select('pSphere1.f[{}]'.format(match), add=True)