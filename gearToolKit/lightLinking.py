import maya.cmds as cmds
import functools

# Add
# Remove
# Difference
# Solo

def add_light_links():
    pass

def reset_light_links(nodes=None, *args):
    if nodes is None:
        nodes = check_selection()
    lights = list_scene_lights()
    cmds.lightlink(light=lights, object=nodes)

def check_selection(size=1, *args):
    selection = cmds.ls(selection=True, long=True)
    if selection:
        if len(selection) < size:
            cmds.error('The selection must contains the source object and at'+
                ' least one target')
        else:
            return selection
    else:
        cmds.error('Selection is empty.')

def is_transform(node, *args):
    if cmds.objectType(node) == 'transform':
        return True
    else:
        return False

def get_light_links(node, *args):
    # Query light links a given node
    return cmds.lightlink(object=node)

def list_scene_lights(*args):
    lights = cmds.ls(type='aiAreaLight', long=True)
    lights = append_relatives(lights)

    return lights

def append_relatives(nodes, *args):
    # Will return shapes and transforms of given nodes
    results = []
    for x in nodes:
        if is_transform(x):
            results.append(cmds.listRelatives(x, shapes=True)[0])
        else:
            results.append(cmds.listRelatives(x, parent=True)[0])
    return nodes+results

def remove_all_links(nodes=None, *args):
    # Will delete all lights links from the objects (objects will not
    # have any direct lighting)
    if nodes is None:
        nodes = check_selection()
    lights = list_scene_lights()

    cmds.lightlink(b=True, light=lights, object=nodes)

def transfer_light_links(*args):
    # Transfer light linking from the first selected object to the rest
    # of the selection
    selection = check_selection(size=2)
    source_links = get_light_links(selection[0])
    targets = selection[1:]
    remove_all_links(targets)
    cmds.lightlink(light=source_links, object=targets)

def build_UI(windowed=True, margin={'top':5,'left':5,'right':5,'bottom':5},
    *args):
    if windowed:
        if cmds.window('lightLink_setup', exists=True):
            cmds.deleteUI('lightLink_setup')
        cmds.window('lightLink_setup', title='Light Linking',
            minimizeButton=False, maximizeButton=False)

    cmds.rowLayout(nc=3)
    cmds.iconTextButton(label='Copy', style='iconAndTextHorizontal',
        image='spotlight.png', width=80,
        command=functools.partial(transfer_light_links),
        annotation='Copy light linking from the first object of the'+
            ' selection to the rest of the selection.')
    cmds.iconTextButton(label='Reset', style='iconAndTextHorizontal',
        command=functools.partial(reset_light_links),
        image='spotlight.png', width=80)
    cmds.iconTextButton(label='Delete', style='iconAndTextHorizontal',
        command=functools.partial(remove_all_links),
        image='spotlight.png', width=80)
    cmds.setParent('..') # Close row layout

    if windowed:
        cmds.showWindow()

build_UI()