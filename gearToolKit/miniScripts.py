'''
Mini scripts by Anthony Tran
'''

import maya.cmds as cmds

def getSelectionCenter():
	bbox = cmds.exactWorldBoundingBox()
	return [ (bbox[3]+bbox[0])/2, (bbox[4]+bbox[1])/2, (bbox[5]+bbox[2])/2 ]

def getSelectionBboxSize():
	bbox = cmds.exactWorldBoundingBox()
	return [ (bbox[3]-bbox[0]), (bbox[4]-bbox[1]), (bbox[5]-bbox[2]) ]

def selectionCenterLocator():
    t = getSelectionCenter()
    a = cmds.spaceLocator()
    cmds.xform(a, absolute=True, t=t )

def downToGrid():
	bbox = cmds.exactWorldBoundingBox()
	cmds.xform(cmds.ls(selection=True), relative=True, t=[0,abs(bbox[1]),0])

def alignToCenter_UI():
	if cmds.window('atc_UI', exists=True):
		cmds.deleteUI('atc_UI')
	cmds.window('atc_UI', title='Align To Center', toolbox=True)
	cmds.columnLayout(adjustableColumn=True)
	cmds.checkBoxGrp(numberOfCheckBoxes=3, label='Align To:',
					label1='X', label2='Y', label3='Z',
					value1=True, value2=True, value3=True,
					cw=[(1,75),(2,30),(3,30),(4,30)])
	cmds.button(label='Apply', command='alignToCenter()')
	cmds.showWindow()

def alignToCenter():
	cmds.deleteUI('atc_UI')

def nurbsCircleToBbox():
	size = getSelectionBboxSize()
	center = getSelectionCenter()
	cmds.circle(radius = size[1]/2, center = center )

def selectShadersRelated():
	sel = cmds.ls(selection=True)
	shaders = []
	meshes = []
	for x in sel:
	    shaders.append( cmds.listConnections( cmds.listRelatives(x, shapes=True), type='shadingEngine')[0] )

	for s in shaders:
	    meshes = cmds.listConnections(s, type="mesh")
	    cmds.select(meshes, add=True)

def removeLambert():
	# Remove lambert from all
	for x in cmds.ls(type='mesh'):
	    SG = cmds.listConnections(x, type='shadingEngine')
	    if SG is not None and SG[0] == 'initialShadingGroup' :
	        cmds.sets(x, remove=SG[0])

def setLambertOnAll():
	# Assign lambert to all
	cmds.sets(cmds.ls(type='mesh'), addElement='initialShadingGroup')