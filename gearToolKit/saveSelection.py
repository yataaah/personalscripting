# Save selection buttons

def ssel_buildUI(mode='windowed'):
    if mode == 'windowed':
        if cmds.window('ssb_window', exists=True):
            cmds.deleteUI('ssb_window')
        cmds.window('ssb_window')
    
    cmds.rowLayout(nc=2)
    cmds.text(label='Quick Selections:')
    nb = 10
    cmds.gridLayout(cw=17,ch=15,nc=nb, nr=1)
    for num in range(nb):
        cmds.button(label=num+1, w=12)
    cmds.setParent('..')
    cmds.setParent('..')

    if mode == 'windowed':
        cmds.showWindow()

ssel_buildUI()