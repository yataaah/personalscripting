# Selection Keeper by Anthony Tran
# 
# TO DO :
# - Store selection as attribute
# - Store selection in UI

import maya.cmds as cmds

def storeSelectionAsAttribute():
    # Query a name for selection
    if cmds.promptDialog(message='Enter a name for selection:', button=['Apply', 'Cancel'], defaultButton='Apply', cancelButton='Cancel') != 'Apply':
        return None
    
    # Get needed variables
    name = cmds.promptDialog(query=True, text=True)
    selection = cmds.ls(selection=True)
    if selection is None:
        cmds.error('Selection is empty.')
    node = selection[0].split('.')[0]

    # Create Attribute
    cmds.addAttr(node, longName=name, dataType='string', hidden=True)
    cmds.setAttr('{}.{}'.format(node, name), ','.join(selection), type='string' ,lock=True)
    
def getSelectionFromAttributes(node, attribute):
    selection = cmds.getAttr(node+'.'+attribute).split(',')

    for element in selection:
        cmds.select(element, add=True)


def storeSelectionInUI():
    pass    

def getSelectionFromUI():
    pass