# Snapping Tools by Anthony Tran
# 
# Snap object to selection center
# Snap object to selection bbox place
# 

import maya.cmds as cmds


def snapToFace():
    pass


def snapToOrigin(side='Y-'):
    # This function will snap a given side of the bounding box of the
    # selection to the origin. Default is Y-, which will seat the
    # object on the grid.
    bbox = cmds.exactWorldBoundingBox()
    sides = {'X+':[-bbox[3],0,0],
             'X-':[abs(bbox[0]),0,0],
             'Y+':[0,-bbox[4],0],
             'Y-':[0,abs(bbox[1]),0],
             'Z+':[0,0,-bbox[5]],
             'Z-':[0,0,abs(bbox[2])]}
    if isinstance(sides, list):
        for side in sides:
            cmds.xform(cmds.ls(selection=True), relative=True, t=sides[side])
    else:
        cmds.xform(cmds.ls(selection=True), relative=True, t=sides[side])

snapToOrigin(side='Y-') 