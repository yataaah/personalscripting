# Set Managerment
# Author : Anthony Tran
# Version 1.0

import maya.cmds as cmds

def get_selection():
    selection = cmds.ls(selection=True)
    if selection:
        return selection
    else:
        cmds.error('No active selection.')

def create_new_set():
    prompt = cmds.promptDialog(message='Enter new set name:',
        button=['Create', 'Cancel'], defaultButton='Create',
        cancelButton='Cancel')
    if prompt == 'Create':
        name = cmds.promptDialog(query=True, text=True)
        cmds.sets(empty=True, name=name)
        sets_to_UI()

def delete_sets():
    selectedSets = cmds.textScrollList('sets_tsl', query=True, selectItem=True)
    cmds.delete(selectedSets)
    sets_to_UI()

def add_to_set():
    selectedSets = cmds.textScrollList('sets_tsl', query=True, selectItem=True)
    selection = get_selection()
    for s in selectedSets:
        cmds.sets(selection, forceElement=s)
    sets_to_UI()

def remove_from_set():
    selectedSets = cmds.textScrollList('sets_tsl', query=True, selectItem=True)
    selection = get_selection()
    for s in selectedSets:
        cmds.sets(selection, remove=s)
    sets_to_UI()

def sets_to_UI():
    selected = cmds.textScrollList('sets_tsl', query=True, selectItem=True)
    cmds.textScrollList('sets_tsl', edit=True, removeAll=True)
    sets = get_sets()
    if selected:
        selected = [x for x in selected if x in sets]
    for s in sets:
        cmds.textScrollList('sets_tsl', edit=True, append=s)
    cmds.textScrollList('sets_tsl', edit=True,
        selectItem=selected)

def get_sets():
    objSets = cmds.ls(type='objectSet')
    sg = cmds.ls(type='shadingEngine')
    for x in ['defaultLightSet', 'defaultObjectSet']:
        objSets.remove(x)
    for x in sg:
        objSets.remove(x)
    return objSets

def setManagement_buildUI(mode='windowed',
    margin={'top':4, 'left':4, 'right':4, 'bottom':4}):

    if mode == 'windowed':
        if cmds.window('setManagementUI', exists=True):
            cmds.deleteUI('setManagementUI')
        cmds.window('setManagementUI', title='Set Management',
            maximizeButton=False, minimizeButton=False)

    f1 = cmds.formLayout()
    cl = cmds.columnLayout(adj=True, width=75)
    cmds.text(label='Sets')
    cmds.button(height=20, label='New Set', command='create_new_set()')
    cmds.button(height=30, label='Delete\nSelected', command='delete_sets()')
    cmds.separator(style='none', height=5)
    cmds.text(label='Objects')
    cmds.button(height=20, label='Add', command='add_to_set()')
    cmds.button(height=20, label='Remove', command='remove_from_set()')
    cmds.setParent('..') # Close button column layout

    bt = cmds.iconTextButton(label='Refresh', image='refreshGray.png',
        sourceType='python', command='sets_to_UI()', width=75,
        backgroundColor=[.22,.22,.22], style='iconAndTextHorizontal', 
        overlayLabelColor=[.4,.4,.4])

    sl = cmds.textScrollList('sets_tsl', width=150, allowMultiSelection=True)
    sets_to_UI()    

    cmds.setParent('..') # Close scroll layout
    cmds.setParent('..') # Close form layout

    cmds.formLayout(f1, edit=True,
        attachForm=[(cl, 'top', margin['top']), 
                    (cl, 'left', margin['left']), 
                    (cl, 'bottom', margin['bottom']),
                    (sl, 'top', margin['top']),
                    (sl, 'right', margin['right']),
                    (sl, 'bottom', margin['bottom']),
                    (bt, 'left', margin['left']),
                    (bt, 'bottom', margin['bottom'])],
        attachControl=[(sl, 'left', 3, cl)])

    if mode == 'windowed':
        cmds.showWindow('setManagementUI')

setManagement_buildUI()